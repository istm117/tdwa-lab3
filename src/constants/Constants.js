export const EMPTY_TODO = "You cannot add empty todo";
export const DUPLICATED_TODO = "You cannot add already existed todo";
export const LIMIT_TODOS = "LIMIT_TODOS";
export const TODO_APPLICATION_VLSU = "TODO_APPLICATION_VLSU";
