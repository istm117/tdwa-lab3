import { combineReducers } from 'redux';
import * as actionsType from '../actions/actionTypes';
import * as constants from '../constants/Constants';

function app(state = initialState, action) {
    let newState = state;
    console.log(action.type);
    switch (action.type) {
        case actionsType.INIT_APP:
            if (localStorage.getItem(constants.TODO_APPLICATION_VLSU) !== null) {
                newState = JSON.parse(localStorage.getItem(constants.TODO_APPLICATION_VLSU));
                if (newState.warning !== constants.LIMIT_TODOS) {
                    newState.warning = '';
                }
            } else {
                if (newState.warning !== constants.LIMIT_TODOS) {
                    newState.warning = '';
                }
            }
            return newState;
        case actionsType.ADD_TODO:
            let todoCount = state.todos.length;
            // Check for empty todo
            if (action.text.trim() === '') {
                newState = {
                    ...state,
                    warning: constants.EMPTY_TODO
                };
                return newState;
            }
            let newTodos = state.todos;
            // Check for duplicating
            let search = newTodos.find(item =>
                item.text === action.text
            );

            if (search !== undefined) {
                newState = {
                    ...state,
                    warning: constants.DUPLICATED_TODO
                };
                return newState;
            } else {
                newTodos.push({
                    id: todoCount++,
                    text: action.text,
                    completed: false,
                    time: 0,
                    created: new Date().getTime()
                });
                newState = {
                    ...newState,
                    warning: ''
                };
                // Check for limit
                if (todoCount === 5) {
                    newState = {
                        ...state,
                        warning: constants.LIMIT_TODOS
                    };
                    return newState;
                }
                return newState;
            }
        case actionsType.SWAP_TODO:
            let updatedTodos = state.todos.map((item) => {
                if (item.id === action.id) {
                    item.completed = !item.completed;
                    let currentDate = new Date().getTime();
                    item.time = currentDate - item.created;
                }
                return item;
            });
            newState = {
                ...state,
                test: state.test+1,
                todos: updatedTodos
            };
            return newState;
        case actionsType.UPDATE_APP:
            localStorage.setItem(constants.TODO_APPLICATION_VLSU, JSON.stringify(newState));
            return newState;
        case actionsType.RETURN_COUNTER:
            return newState.todos.length;
        case actionsType.RECEIVE_TODOS:
            newState = {
                ...state,
                todos: action.todos,
                warning: ''
            };
            return newState;
        default:
            return state;
    }
}

const initialState = {
    todos: [],
    warning: "warning",
    test: 0
};

const todoApp = combineReducers({
    app
});

export default todoApp