import { connect } from 'react-redux'
import { swapTodo, updateApp } from '../actions/actions'
import TodoList from '../components/TodoList'

const mapStateToProps = state => {
    return {
        todos: state.app.todos,
        test: state.app.test
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onTodoClick: id => {
            dispatch(swapTodo(id));
            dispatch(updateApp());
        }
    }
};

const TodoListWrapper = connect(
    mapStateToProps,
    mapDispatchToProps
)(TodoList);

export default TodoListWrapper