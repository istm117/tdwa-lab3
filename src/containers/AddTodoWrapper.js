import { connect } from 'react-redux'
import { addTodo, updateApp } from '../actions/actions'
import AddTodo from '../components/AddTodo'

const mapStateToProps = state => {
    return {
        counter: state.app.todos.length,
        warning: state.app.warning
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addTodoClick: text => {
            dispatch(addTodo(text));
            dispatch(updateApp());
        }
    }
};

const AddTodoWrapper = connect(
    mapStateToProps,
    mapDispatchToProps,
)(AddTodo);

export default AddTodoWrapper