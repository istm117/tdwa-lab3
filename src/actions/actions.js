import * as types from './actionTypes';
import axios from 'axios';

export function receiveTodos(json) {
    return {
        type: types.RECEIVE_TODOS,
        todos: json
    }
}

export function requestTodos() {
    return {
        type: types.REQUEST_TODOS,
    }
}

export function fetchTodos() {
    return function (dispatch) {
        dispatch(requestTodos());
        return axios({
            method:'get',
            url:'http://localhost:3001/getTodos',
        }).then(function (response) {
            dispatch(receiveTodos(response.data.todos));
        });
    }
}

export function addTodo(text) {
    return {
        type: types.ADD_TODO,
        text
    }
}

export function sendTodo(text) {
    return function (dispatch) {
        dispatch(addTodo(text));
        return axios({
            method:'post',
            url:'http://localhost:3001/saveTodo',
            data: {
                text
            }
        }).then(function (response) {
            dispatch(receiveTodos(response.data.todos));
        });
    }
}

export function swapTodo(id) {
    return {
        type: types.SWAP_TODO,
        id
    }
}

export function initApp() {
    return {
        type: types.INIT_APP
    }
}

export function returnCounter() {
    return {
        type: types.RETURN_COUNTER
    }
}

export function updateApp() {
    return {
        type: types.UPDATE_APP,
    }
}