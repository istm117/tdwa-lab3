export const ADD_TODO = "ADD_TODO";
export const SWAP_TODO = "SWAP_TODO";
export const INIT_APP = "INIT_APP";
export const RETURN_COUNTER = "RETURN_COUNTER";
export const UPDATE_APP = "UPDATE_APP";
export const REQUEST_TODOS = "REQUEST_TODOS";
export const RECEIVE_TODOS = "RECEIVE_TODOS";