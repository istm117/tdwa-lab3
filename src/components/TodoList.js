import React, {Component} from 'react';
import Todo from './Todo';
import Counter from './Counter';

class TodoList extends Component {
    render() {
        console.log('todos in component');
        console.log(this.props.todos);
        return (
            <div id="todos" className="col-12 d-flex justify-content-center">
                <div className="todoWrapper col-6 flex-column justify-content-center">
                    {

                        this.props.todos.map((todo) =>
                            <Todo key={todo.id} todo={todo} onClick={() => this.props.onTodoClick(todo.id)}/>
                        )
                    }
                    <div className="d-flex justify-content-center">
                        <Counter counter={this.props.todos.length}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default TodoList