import React from 'react';

const Todo = ({ onClick, todo }) =>
{
    let date = new Date(todo.created);
    let time = new Date(todo.time);
    let stringDate = date.getHours() + ':';
    stringDate = stringDate.concat(date.getMinutes() + ':');
    stringDate = stringDate.concat(date.getSeconds() + ' ');
    stringDate = stringDate.concat(date.getDate() + '.');
    stringDate = stringDate.concat((date.getMonth()+1) + '.');
    stringDate = stringDate.concat(date.getFullYear());

    let stringTime = time/1000;

    return (
        <div className={"todoItem mb-3 p-3" + (todo.completed ? ' completedTodo' : ' activeClass')}>
            <div className="todoItemBadges">
                <div className="badge badge-default">{stringDate}</div>
                <div className="badge badge-success ml-1">{stringTime > 0 ? stringTime : ''}</div>
            </div>
            <div className="card-body">
                {todo.text}
            </div>
            <div className="todoActions">
                <div className={"todoAction todoDone" + (todo.completed ? ' innertElement' : '')}
                     onClick={todo.completed ? null : onClick}>&#10004;</div>
            </div>
        </div>
    )
}

export default Todo