import React from 'react';

const warning = ({warning}) =>  {
    return (
        <div className="alert alert-warning" role="alert">
            {warning}
        </div>
    )
};

export default warning