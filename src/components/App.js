import React, {Component} from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import todoApp from '../reducers/reducers';
import {fetchTodos, sendTodo} from '../actions/actions';
import TodoListWrapper from '../containers/TodoListWrapper';
import AddTodoWrapper from '../containers/AddTodoWrapper';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import thunkMiddleware from 'redux-thunk'

const store = createStore(
    todoApp,
    applyMiddleware(thunkMiddleware)
);

const Header = () => {
    return (
        <div>
            <div className="d-flex justify-content-center mt-3">
                <div className="col-6">
                    <div className="col-12 d-flex justify-content-center mb-3">
                        <h1>Todos</h1>
                    </div>
                    <div className="col-12 d-flex flex-row mb-3">
                        <div className="col-3">
                            <Link className="btn btn-basic" to='/'>Home</Link>
                        </div>
                        <div className="col-3">
                            <Link className={"btn btn-info" + (store.getState().app.todos.length === 3 ? ' disabled' : '')}
                                to='/addTodo'>Add todo</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};


const Root = () => {
    store.dispatch(fetchTodos());
    store.dispatch(sendTodo());
    return (
        <Provider store={store}>
            <Router>
                <div className="d-flex flex-column">
                    <Header/>
                    <Route exact path="/" component={TodoListWrapper} />
                    <Route path="/addTodo" component={AddTodoWrapper} />
                </div>
            </Router>
        </Provider>
    )
};

export default class App extends Component {
    render() {
        return (
            <Root/>
        );
    }
}