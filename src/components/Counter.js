import React, {Component} from 'react';

export default class Counter extends Component {
    render() {
        return (
            <div>
                <h4>Current count of todos: {this.props.counter}</h4>
            </div>
        );
    }
}