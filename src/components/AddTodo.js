import React, {Component} from 'react';
import Warning from '../components/Warning';
import { Redirect } from 'react-router-dom';
import Counter from '../components/Counter';

class AddTodo extends Component {
    constructor(props) {
        super();
        this.state = {
            counter: props.counter,
            addTodoClick: props.addTodoClick,
            warning: props.warning
        }
    }

    render() {
        let input;
        return (
            <div className="col d-flex flex-column align-items-center">
                <div className="col-6">
                    {this.props.warning !== '' ? <Warning warning={this.props.warning}/> : ''}
                    {this.props.counter === 3 ? <Redirect to="/"/> : ''}
                    <form onSubmit={e => {
                            e.preventDefault();
                                this.props.addTodoClick(input.value);
                                input.value = '';
                            }}>
                        <input className="form-control w-100 d-inline" ref={node => {
                                input = node
                        }}/>
                        <button className="btn btn-success w-100 mt-3 mb-3 d-inline" type="submit">
                            Add todo
                        </button>
                    </form>
                </div>
                <Counter counter={this.props.counter}/>
            </div>
        )
    };
}

export default AddTodo

